# EKS Deployment



## Getting started

In this repository, we are going to do the followings:

1. Create a new AWS EKS cluster which will use an existing VPC and existing public subnet(s).
2. Create an IAM role with S3 access permissions.
3. Create a Kubernetes service account linked to the previously established IAM role.
4. Deploy a sample nginx pod using the kubernetes service account to interact with S3.

## Prerequisites:

- Terraform >= 1.7
- Admin access to AWS account where we are going to create a new infrastructure.
- Existing VPC and at least one public subnet group.

## Create infrastructure
- After cloning the repository, run the following command:

```
cd eks_deployment
terraform init
```

- Open `terraform.tfvar` file and update values for the following variables:
* `access_key`
* `secret_key`
* `vpc_id`
* `public_subnets`

- Update other variables value to meet your requirements.
- Run the following commands:
```
terraform plan
terraform apply
```
## Create a pod which has an access to S3

-  Refer to `kubernetes.tf` file where we are using kubernetes provider to create Kubernetes Service Account and simple nginx pod which has this service account assigned in order to have access on S3. You should update `kubernetes.tf` file to meet you requirements and run `terraform apply` again.
variable "region" {
  description = "AWS region"
  type        = string
  default     = "eu-west-1"
}

variable "access_key" {
  description = "AWS access key"
  type        = string
}

variable "secret_key" {
  description = "AWS secret key"
  type        = string
}

variable "cluster_name" {
  description = "AWS eks cluster name"
  type        = string
  default     = "Default-EKS"
}

variable "cluster_version" {
  description = "EKS cluster version"
  type        = string
  default     = "1.29"
}

variable "vpc_id" {
  description = "Existing VPC id"
  type        = string
}

variable "public_subnets" {
  description = "List of public subnets"
  type        = list(string)
}

variable "ami_type" {
  description = "EC2 ami type"
  type        = string
  default     = "AL2_x86_64"
}


variable "node_groups" {
  description = "AWS node groups"
  type = list(object({
    name                  = string
    instance_types        = list(string)
    volume_size           = number
    volume_type           = string
    device_name           = string
    iops                  = number
    throughput            = number
    delete_on_termination = bool
    min_size              = number
    max_size              = number
    desired_size          = number
  }))
  default = [{
    name                  = "default-node-group"
    instance_types        = ["t3a.medium"]
    volume_size           = 25
    volume_type           = "gp3"
    device_name           = "/dev/xvda"
    iops                  = 3000
    throughput            = 125
    delete_on_termination = true
    min_size              = 1
    max_size              = 3
    desired_size          = 2
  }]
}

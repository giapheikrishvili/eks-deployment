provider "aws" {
  region     = var.region
  access_key = var.access_key
  secret_key = var.secret_key
}

module "eks" {
  source  = "terraform-aws-modules/eks/aws"
  version = "20.8.4"

  cluster_name    = var.cluster_name
  cluster_version = var.cluster_version

  vpc_id                                   = var.vpc_id
  subnet_ids                               = var.public_subnets
  cluster_endpoint_public_access           = true
  enable_cluster_creator_admin_permissions = true

  eks_managed_node_group_defaults = {
    ami_type = var.ami_type
  }

  eks_managed_node_groups = {
    for k, v in var.node_groups : v.name => {
      name           = v.name
      instance_types = v.instance_types
      block_device_mappings = {
        xvda = {
          device_name = v.device_name
          ebs = {
            volume_size           = v.volume_size
            volume_type           = v.volume_type
            iops                  = v.iops
            throughput            = v.throughput
            delete_on_termination = v.delete_on_termination
          }
        }
      }
      min_size     = v.min_size
      max_size     = v.max_size
      desired_size = v.desired_size
    }
  }
}

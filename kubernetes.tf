data "aws_eks_cluster_auth" "this" {
  name = "${module.eks.cluster_name}"
}

provider "kubernetes" {
  host                   = module.eks.cluster_endpoint
  cluster_ca_certificate = base64decode(module.eks.cluster_certificate_authority_data)
  token                  = data.aws_eks_cluster_auth.this.token
}


# Associate IAM Role with Kubernetes Service Account
resource "kubernetes_service_account" "s3_service_account" {
  metadata {
    name      = "s3-sa"
    namespace = "default"
    annotations = {
      "eks.amazonaws.com/role-arn" = aws_iam_role.s3_role.arn
    }
  }
}

# Run simple nginx pod which will have access to S3
resource "kubernetes_pod" "nginx" {
  metadata {
    name      = "nginx"
    namespace = "default"
  }

  spec {
    # Specify kubernetes service account to get access to S3
    service_account_name = kubernetes_service_account.s3_service_account.metadata[0].name
    container {
      image = "nginx:1.21.6"
      name  = "nginx"

      env {
        name  = "environment"
        value = "test"
      }

      port {
        container_port = 80
      }

      liveness_probe {
        http_get {
          path = "/"
          port = 80

          http_header {
            name  = "X-Custom-Header"
            value = "Awesome"
          }
        }

        initial_delay_seconds = 3
        period_seconds        = 3
      }
    }
  }
  depends_on = [
    kubernetes_service_account.s3_service_account
  ]
}

access_key      = "YourAccessKey"
secret_key      = "YourSecretKey"
vpc_id          = "YourVPCId"
public_subnets  = ["YourSubnetId"]
region          = "eu-west-1"
cluster_name    = "demo-eks"
cluster_version = "1.29"


node_groups = [
  {
    name                  = "node-group-1"
    instance_types        = ["t3a.medium"]
    volume_size           = 30
    volume_type           = "gp3"
    device_name           = "/dev/xvda"
    iops                  = 3000
    throughput            = 125
    delete_on_termination = true
    min_size              = 1
    max_size              = 4
    desired_size          = 2
  },
  {
    name                  = "node-group-2"
    instance_types        = ["t3a.xlarge"]
    volume_size           = 40
    volume_type           = "gp3"
    device_name           = "/dev/xvda"
    iops                  = 3000
    throughput            = 125
    delete_on_termination = true
    min_size              = 1
    max_size              = 4
    desired_size          = 2
  }
]


# Create S3 Policy
resource "aws_iam_policy" "s3_policy" {
  name        = "${var.cluster_name}-s3-policy"
  description = "S3 full access IAM policy"
  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Effect = "Allow"
        Action = [
          "s3:*"
        ]
        Resource = "*"
      }
    ]
  })
}

# Create IAM Role
resource "aws_iam_role" "s3_role" {
  name = "${var.cluster_name}-s3-role"
  assume_role_policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Effect = "Allow",
        Principal = {
          Federated = "${module.eks.oidc_provider_arn}"
        },
        Action = "sts:AssumeRoleWithWebIdentity",
        Condition = {
          StringEquals = {
            "${module.eks.oidc_provider}:aud" : "sts.amazonaws.com"
          }
        }
      }
    ]
  })
  depends_on = [
    module.eks
  ]
}

# Attach Policy to IAM Role
resource "aws_iam_role_policy_attachment" "policy_attachment" {
  role       = aws_iam_role.s3_role.name
  policy_arn = aws_iam_policy.s3_policy.arn
  depends_on = [
    aws_iam_policy.s3_policy,
    aws_iam_role.s3_role
  ]
}
